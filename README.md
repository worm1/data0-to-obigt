# data0 to obigt

Under development.

Converting existing EQ3 data0 files into WORM's database format.

Produces three files readable by the data0create code: 1) a CHNOSZ-compatible OBIGT csv, 2) a csv containing EQ3-relevant parameters like azero, and 3) a csv with solid solution parameters.

Confirm this works by placing data0_to_obigt.ipynb and data0.xyz in the same directory. Open and run data0_to_obigt.ipynb in Jupyter Notebooks. Edit the first cell in the notebook to specify the three letter code of a custom data0 file.